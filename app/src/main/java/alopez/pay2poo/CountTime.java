package alopez.pay2poo;

import android.annotation.SuppressLint;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by ALopez on 11/21/2016.
 */

public class CountTime extends AppCompatActivity {

    private TextView hourlyCounter;

    Chronometer chronometer;
    Double salary = 0.00;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count_time);

        final double minutely = getIntent().getDoubleExtra("minutely", -1);
        boolean storeInfo = getIntent().getBooleanExtra("storeInfo", false);
        hourlyCounter = (TextView) findViewById(R.id.hourly_counter);
        BigDecimal bd = new BigDecimal(salary);
        bd = new BigDecimal(salary).setScale(2, RoundingMode.HALF_EVEN);
        salary = bd.doubleValue();
        hourlyCounter.setText("$" + salary.toString());

        chronometer = (Chronometer) findViewById(R.id.chronometer2);
        chronometer.start();
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                System.out.println(getElapsedTime());
                System.out.println(getElapsedTime()/1000);
                System.out.println(getElapsedTime());
                if ((getElapsedTime()/1000)%60 == 0) {
                    salary += minutely;
                    BigDecimal bd = new BigDecimal(salary);
                    bd = new BigDecimal(salary).setScale(2, RoundingMode.HALF_EVEN);
                    salary = bd.doubleValue();
                    hourlyCounter.setText("$" + salary.toString());
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        chronometer.stop();
    }

    @Override
    public void onDestroy() {
        chronometer.stop();
    }

    private long getElapsedTime() {
        long elapsedMillis = SystemClock.elapsedRealtime() - chronometer.getBase();
        return elapsedMillis;
    }
}
