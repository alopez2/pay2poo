package alopez.pay2poo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class StartPage extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_start_page);
        final String storeInfoToDB = "storeInfo";
        Button saveInfo = (Button) findViewById(R.id.save_info);
        Button oneTime = (Button) findViewById(R.id.one_time);

        saveInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        oneTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent changeActivity = new Intent(StartPage.this, SetUpPage.class);
                changeActivity.putExtra(storeInfoToDB, false);
                StartPage.this.startActivity(changeActivity);
            }
        });

    }
}
