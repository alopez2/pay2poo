package alopez.pay2poo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

/**
 * Created by ALopez on 11/21/2016.
 */

public class SetUpPage extends AppCompatActivity {
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_page);

        context = this;
        final Button cont = (Button) findViewById(R.id.start);
        final CheckBox partTime = (CheckBox) findViewById(R.id.part_time);
        final EditText salary = (EditText) findViewById(R.id.salary);
        View.OnClickListener listener = null;
        final boolean storeInfo = getIntent().getBooleanExtra("storeInfo", false);

        partTime.setClickable(true);

        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userSalary = salary.getText().toString().trim();
                double minutely = -1;
                boolean partTimeChecked = partTime.isChecked();
                if(userSalary == null || userSalary.isEmpty()) {
                    Toast.makeText(context, "Please enter a salary.", Toast.LENGTH_SHORT).show();
                } else {
                    minutely = calculateminutely(partTimeChecked, Double.parseDouble(userSalary));
                }
                Intent changeActivity = new Intent(SetUpPage.this, CountTime.class);
                changeActivity.putExtra("storeInfo", storeInfo);
                changeActivity.putExtra("minutely", minutely);
                SetUpPage.this.startActivity(changeActivity);
            }
        });

    }

    private double calculateminutely(boolean partTime, double userSalary) {
        double salary = -1;
        if (partTime) {
            salary = (userSalary/1040)/60;
        } else {
            salary = (userSalary/2080)/60;
        }
        return salary;
    }

}

